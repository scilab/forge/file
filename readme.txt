File Toolbox

Purpose
-------

The goal of this toolbox is to provide a collection of 
functions to manage files. 
The main objective is to provide a more consistent API than 
Scilab does. 

For example the Scilab/fullfile function is of limited use. 
Indeed, if the various parts of a file are gathere in a matrix of 
strings pathmatrix, the calling sequence fullfile(pathmatrix) will not work. 
By contrast, the calling sequence fullfile(path1,path2,path3) can 
be straighforwardly converted to the call file_join([path1,path2,path3]).

The file_tempfile function is not provided by Scilab. 
It is often used in unit tests, where we need to create 
unique temporary files.

The Scilab/copyfile is renamed into file_copy, which is consistent with 
the naming convention chosen in the current module.
By contrast, Scilab uses a weird naming convention: the function, Scilab/fileext
begins by "file", while Scilab/deletefile ends by "file.

Features
-------

The following is a list of the current functions :

 * file_join : Join sub-paths from a matrix of strings.
 * file_nativename : Returns the platform-specific name of a path.
 * file_normalize : Returns a unique normalized path.
 * file_pathtype : Returns the type of a path.
 * file_split : Split the path into a matrix of strings.
 * file_tempfile : Returns the name of a temporary file name suitable for writing.
 * file_touch : Alter the atime and mtime of the specified files.

Bibliography
------------

 * http://www.tcl.tk/man/tcl8.4/TclCmd/file.htm
 * http://flibs.cvs.sourceforge.net/viewvc/flibs/src/filedir/m_vfile.f90?view=markup
 * http://flibs.cvs.sourceforge.net/viewvc/flibs/tests/filedir/test_m_filedir.f90?view=markup

TODO 
----
  * integrate, rename and redesign the interesting functions from Scilab.
  * This module should depend on the apifun module.
  * Add unit tests.
  * Add file_copy (from Scilab/copyfile)
  * Add file_delete (from Scilab/deletefile and Scilab/removedir and Scilab/rmdir)
  * Add file_dirname
  * Add file_isfile
  * Add file_isdir
  * Add file_extension
  * Add file_executable
  * Add file_mkdir (from Scilab/createdir)
  * Add file_mtime (from Scilab/fileinfo)
  * Add file_rename
  * Add file_size
  * Add file_stat
  * Add file_tail
  * Add file_volumes (from Scilab/getdrives)
  * Add file_writable

Author
------

 * Copyright (C) 2010 - DIGITEO - Michael Baudin
 * Copyright (c) 2008 - Michael Baudin
 * Copyright (c) 2008 - Arjen Markus

Licence
-------

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



