// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if ( and ( computed==expected ) ) then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

path = file_normalize("/foo/bla/myfile.txt");
expected = "/foo/bla/myfile.txt";
assert_equal ( path , expected );
//
path = file_normalize("\foo\bla\myfile.txt");
expected_Windows = "/foo/bla/myfile.txt";
expected_Linux = pwd()+"/\foo\bla\myfile.txt";
if ( getos()=="Windows") then
  assert_equal ( path , expected_Windows );
else
  assert_equal ( path , expected_Linux );
end
//
// Check "."
path = file_normalize(".");
expected = pwd();
assert_equal ( path , expected );
//
// Check ".."
path = file_normalize("..");
cwd = pwd();
k = file_lastsepindex(cwd);
expected = part(cwd,1:k-1);
assert_equal ( path , expected );
//
// Check ".."
path = file_normalize("/foo1/foo2/../myfile.txt");
expected = "/foo1/myfile.txt";
assert_equal ( path , expected );
//
// Check "."
path = file_normalize("/foo1/foo2/./myfile.txt");
expected = "/foo1/foo2/myfile.txt";
assert_equal ( path , expected );
//
// Check "."
path = file_normalize("./myfile.txt");
expected = pwd()+"/myfile.txt";
assert_equal ( path , expected );
//
// Check "." and ".."
path = file_normalize("./foo/../myfile.txt");
expected = pwd()+"/myfile.txt";
assert_equal ( path , expected );

