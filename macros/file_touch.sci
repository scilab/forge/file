// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



function file_touch ( name )
    // Alter the atime and mtime of the specified files. 
    // 
    // Calling Sequence
    // file_touch ( name )
    //
    // Parameters
    // name : a 1-by-1 matrix of strings
    //
    // Description
    // Creates the file if it does not exist. 
    // Alter the atime and mtime of the specified files. 
    // 
    // Examples
    // name = file_tempfile (  )
    // file_touch ( name )
    // x = fileinfo ( name );
    // mprintf("Last modification: %d\n",x(6))
    // //
    // // Wait for 1 second
    // realtimeinit(1);//sets time unit to half a second
    // realtime(0);//sets current date to 0
    // realtime(1);
    // //
    // // Touch it again
    // file_touch ( name )
    // x = fileinfo ( name );
    // mprintf("Last modification: %d\n",x(6))
    // 
  // 
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (c) 2008 - Michael Baudin
  // Copyright (c) 2008 - Arjen Markus

    if ( fileinfo(name) <> [] ) then
        os = getos()
        if ( os=="Windows" ) then
            dos("copy /b "+name+" +,, "+name)
        else
            unix_g("touch "+name)
        end
    else
        // If the file does not exist, create it as an empty file
        [fd,err]=mopen(name,"w")
        if ( err <> 0 ) then
            error(msprintf("%s: Failed to open file %s","file_touch",name))
        end
        err=mclose(fd)
        if ( err <> 0 ) then
            error(msprintf("%s: Failed to close file %s","file_touch",name))
        end
    end
endfunction

