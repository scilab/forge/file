// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function tf = file_isdirectory ( name )
    // Returns %t if the path is a directory.
    // 
    // Calling Sequence
    // name : a 1-by-1 matrix of strings
    // tf : a 1-by-1 matrix of booleans
    //
    // Description
    // Returns %t if the name is a directory, %f if not.
    //
    // Examples
    //   file_isdirectory ( file_join([SCI,"etc","scilab.start"]) ) // %f
    //   file_isdirectory ( TMPDIR ) // %t
    // 
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin

    tf = isdir(name)
endfunction

