// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function t = file_atime ( name )
  // Returns the access time of the file.
  // 
  // Calling Sequence
  // t = file_atime ( name )
  //
  // Parameters
  // name : a 1-by-1 matrix of strings
  // t : a 1-by-1 matrix of floating point integers
  //
  // Description
  // Returns the access time of the file.
  // 
  // Examples
  // name = file_tempfile (  )
  // file_touch(name)
  // t = file_atime(name)
  // 
  // 
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (c) 2008 - Michael Baudin
  // Copyright (c) 2008 - Arjen Markus

  [x,ierr] = fileinfo(name)
  if ( ierr <> 0 ) then
    localstr = gettext("%s: Failed to get stat for file %s")
    error (msprintf(localstr,"file_atime",name))
  end
  t = x(8)
endfunction

