// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function i = file_firstsepindex ( name )
  //
  // Returns the index of the first separator.

  //
  // Search for platform-specific separator
  sep = file_separator()
  for k = 1 : length(name)
    if ( part(name,k:k)==sep ) then
      i = k
      return
    end
  end
  //
  // Search for canonical separator
  sep = "/"
  for k = 1 : length(name)
    if ( part(name,k:k)==sep ) then
      i = k
      return
    end
  end
  i = []
endfunction

