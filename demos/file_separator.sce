//
// This help file was automatically generated from file_separator.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_separator.sci
//

file_separator()
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_separator.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
