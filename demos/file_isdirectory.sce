//
// This help file was automatically generated from file_isdirectory.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_isdirectory.sci
//

file_isdirectory ( file_join([SCI,"etc","scilab.start"]) ) // %f
file_isdirectory ( TMPDIR ) // %t
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_isdirectory.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
