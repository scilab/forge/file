//
// This help file was automatically generated from file_pathtype.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_pathtype.sci
//

name = file_tempfile (  )
file_touch ( name )
file_pathtype ( name ) // Should be 1
//
file_pathtype ( "foo.txt" ) // Should be 2
//
volumematrix = getdrives()
file_pathtype ( volumematrix(1) ) // Should be 1
//
file_pathtype ( "/" ) // Should be 3 on Windows, 1 on Linux
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_pathtype.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
