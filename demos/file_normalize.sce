//
// This help file was automatically generated from file_normalize.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_normalize.sci
//

file_normalize ( "/foo/myfile.txt" )
expected = "/foo/bla/myfile.txt";
//
file_normalize ( "\foo\myfile.txt" )
expected = "/foo/bla/myfile.txt";
//
file_normalize ( "." )
expected = pwd()
//
file_normalize ( ".." )
//
file_normalize ( "../file.txt" )
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_normalize.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
